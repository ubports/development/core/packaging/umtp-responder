# umtp-responder
This package is not applicable for Ubuntu Touch on 16.04. On 16.04, [mtp-server](https://gitlab.com/ubports/development/core/mtp)
is used instead and no further backporting work will be done.

This branch is created to prevent CI for building this against 16.04.

